Components.utils.import("chrome://redirector/content/js/redirectorprefs.js");
Components.utils.import("chrome://redirector/content/js/redirector.js");

var RedirectorOverlay = {
	strings		: null,
	prefs		: null,

	onLoad : function(event) {
		this.strings = document.getElementById("redirector-strings");
		this.prefs = new RedirectorPrefs();
		this.changedPrefs(this.prefs);
		this.prefs.addListener(this);
		document.addEventListener('keypress', function(event) {
			if ((event.charCode == 114) && event.altKey) { //alt+r
				RedirectorOverlay.toggleEnabled();
			}
		}, true);
	},

	onUnload : function(event) {
		this.prefs.dispose();
		Redirector.debug("Finished cleanup");
	},

	changedPrefs : function(prefs) {
		this.refreshUI();
	},

	refreshUI: function() {
		let button = document.getElementById("redirector-button");
		let item = document.getElementById("redirector-enabled-item");
		if (this.prefs.enabled) {
			button.removeAttribute('disabled');
			item.setAttribute('checked', true);
		} else {
			button.setAttribute('disabled', 'yes');
			item.setAttribute('checked', false);
		}
	},

	toggleEnabled: function(event) {
		this.prefs.enabled = !this.prefs.enabled;
		this.refreshUI();
	},

	openSettings : function() {
		gBrowser.selectedTab = gBrowser.addTab("chrome://redirector/content/redirector.html");
	},

	updateTooltip: function(event) {
		AddonManager.getAddonByID("url-rewriter@papush", (addon) => {
			let label = event.target.getElementsByClassName("redirector-tooltip-version")[0];
			label.setAttribute("value", "URL Rewriter " + addon.version);
		});
	}
};

window.addEventListener("load", function(event) { RedirectorOverlay.onLoad(event); }, false);
window.addEventListener("unload", function(event) { RedirectorOverlay.onUnload(event); }, false);
