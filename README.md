This is a fork of an old version of [Einar Egilsson's Redirector
extension](http://einaregilsson.com/redirector) for use in Pale Moon.

Description of the extension from his website:

The add-on lets you create redirects for specific webpages, e.g.
always redirect http://bing.com to http://google.com. It was
originally done by request for someone on the Mozillazine forums. The
redirect patterns can be specified using regular expressions or simple
wildcards and the resulting url can use substitutions based on
captures from the original url. The add-on can for example be used to
redirect a site to its https version, redirect news paper articles to
their print versions, redirect pages to use specific proxy servers and
more.
